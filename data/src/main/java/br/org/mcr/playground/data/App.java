package br.org.mcr.playground.data;

import java.time.LocalDate;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    private static List<LocalDate> generator;

	public static void main( String[] args )
    {
    	
    	LocalDate data = LocalDate.of(2017, 3, 28);
    	long startTime =  System.currentTimeMillis();
    	System.out.println( "Inicio:"+startTime );
    	
    	for (int i = 0; i < 13; i++) {
    		generator = DataGenerator.generator(data, data.plusDays(120));
    		generator.forEach(d -> System.out.println(d));
		}
    	long endTime =  System.currentTimeMillis();
    	System.out.println("Fim:"+endTime );
    	System.out.println("Tempo:"+(endTime-startTime)+" ms");
    	
    }
}
