package br.org.mcr.playground.data;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DataGenerator {

	public static List<LocalDate> generator(LocalDate dataInicial, LocalDate dataFinal) {

		DayOfWeek dayOfWeek = dataInicial.getDayOfWeek();
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		int weekNumberActual = dataInicial.get(weekFields.weekOfWeekBasedYear());
		System.out.println(dataInicial);
		System.out.println("Dia da Semana " + dayOfWeek + " - da semana:" + weekNumberActual);

		LocalDate dataInicialAjustada = dataInicial.minusYears(1);
		DayOfWeek dayOfWeekAjustada = dataInicialAjustada.getDayOfWeek();
		int weekNumberAjustado = dataInicialAjustada.get(weekFields.weekOfWeekBasedYear());
		System.out.println(dataInicialAjustada);
		System.out.println("Dia da Semana " + dayOfWeekAjustada + " - da semana: " + weekNumberAjustado);

		
		int days = 1;

		if (dataInicialAjustada.plusDays(1).isLeapYear() && dataInicialAjustada.plusDays(1).getMonth() == Month.FEBRUARY
				&& dataInicialAjustada.plusDays(1).getDayOfMonth() == 29) {
			days=2;
		}
		
		LocalDate dataInicialAjustadaFinal = dataInicialAjustada.plusDays(days);
		
		
		
		DayOfWeek dayOfWeekAjustadaFinal = dataInicialAjustadaFinal.getDayOfWeek();
		int weekNumberAjustadoFinal = dataInicialAjustadaFinal.get(weekFields.weekOfWeekBasedYear());
		System.out.println(dataInicialAjustadaFinal);
		System.out.println("Dia da Semana " + dayOfWeekAjustadaFinal + " - da semana: " + weekNumberAjustadoFinal);

		// Or use a specific locale, or configure your own rules

		long numOfDaysBetween = ChronoUnit.DAYS.between(dataInicialAjustadaFinal, dataFinal.minusYears(1));
		return IntStream.iterate(0, i -> i + 1).limit(numOfDaysBetween)
				.mapToObj(i -> dataInicialAjustadaFinal.plusDays(i)).collect(Collectors.toList());

	}

}
